(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Eclipse IDE

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter):Nikita Padmannavar
(Course Site):lynda.com
(Course Name):Up and Running with Eclipse
(Course URL):http://www.lynda.com/Eclipse-tutorials/Introducing-Eclipse-debugger/111243/118397-4.html
(Discipline):Technical
(ENDIGNORE)

(Type): truefalse
(Category): 23
(Grade style): 0
(Random answers): 1
(Question): .class file is a binary file and is same as hidden file in an explorer.
(A): True
(B): False 
(Correct): A
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

### A .class file is the binary file coresponding to a java class file (ie test.class corresponds to test.java) but is not by default a hidden file


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question): What does an asterisk beside the file name denote?
(A): The file is saved
(B): The file has unsaved information
(C): The file is imported
(D): The file is exported
(Correct): B
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question): Eclipse helps us write boiler codes such as 
(A): toString method
(B): getters and setters
(C): equals method
(D): all of the above
(Correct): D
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question):  What are modern versions of Eclipse composed of?
(A): Plug-ins
(B): OSGi bundles
(C): None
(Correct): B
(Points): 1
(CF): Modern versions of Eclipse are composed of OSGi bundles. Older versions of Eclipse were composed of modules called plug-ins.
(WF): Modern versions of Eclipse are composed of OSGi bundles. Older versions of Eclipse were composed of modules called plug-ins.
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):23
(Grade style): 0
(Random answers): 1
(Question): Apache Maven is a software project management tool.
(A): True
(B): False
(Correct): A
(Points): 1 
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question): What is refactor used for?
(A): Renaming a variable at one place
(B): Renaming a variable everywhere in the code
(C): Can be used for both
(D): None of the above
(Correct): B
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category):23
(Grade style): 0
(Random answers): 1
(Question): What does the Syntax checking feature do?
(A): Always resolves the problem for us
(B): Resolves the problem for us only once in the code
(C): Sometimes resolves the problem for us
(D): None of the above
(Correct): C
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category):23
(Grade style): 1
(Random answers): 1
(Question): Which of the following are true?
(A): A Java requirement is that all Java-class files exist within a namespace
(B): Java files do not require to exist within a package
(C): Creating a package does not affect the namespace
(D): The namespace is encapsulated by a package
(Correct): A, D
(Points): 1 
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category):23
(Grade style): 0
(Random answers): 1
(Question): The Eclipse internal search system is helps to get all the locations for variables, methods and classes.
(A): True
(B): False
(Correct): A
(Points): 1
(STARTIGNORE)
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multipleselect
(Category):23
(Grade style): 1
(Random answers): 1
(Question): Which of the following is true? (choose 2)
(A): Step Over means if you're using a method, execute the entire method, and go to the line after the call to that method.
(B): Step Into means if you're using a method, execute the entire method, and go to the line after the call to that method.
(C): Step Into means that if you're using a method, step into the code within that method.
(D): Step Over means that if you're using a method, step into the code within that method.
(Correct): A, C
(Points): 1
(STARTIGNORE)
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)