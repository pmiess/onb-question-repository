(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): If a Perscribed Event is skipped (Select all that apply):
(A): This is ok because Agile means this can be decided by the team
(B): This could result in reduced transparency, jeopardizing the opportunity to inspect and adapt
(C): It can be usually remedied by doing two next time
(D): The Scrum Master is accountable
(Correct): B,D
(Points): 1
(CF): The Scrum Master is accountable for Perscribed Events such as Sprint Reviews and Retrospectives.
(WF): The Scrum Master is accountable for Perscribed Events such as Sprint Reviews and Retrospectives.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5 
(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What is the role of the Project Manager in the Agile methodology?
(A): Make all key decisions
(B): Guide and clear roadblocks for the team
(C): Develop effective unit tests
(D): Estimate tasks
(E): Maintain rigid control over the team
(Correct): B
(Points): 1
(CF): Project managers should observe, guide, and clear roadblocks for the development team, which requires giving up some level of control as compared to other development methods.
(WF): Project managers should observe, guide, and clear roadblocks for the development team, which requires giving up some level of control as compared to other development methods.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)



(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some important adjustments that can come out of the adapt phase?
(A): Adding or removing features
(B): Re-prioritizing features
(C): Changing the product owner
(D): Updating the project budget
(Correct): A,B
(Points): 1
(CF): The product owner and budget tend to be locked in pretty solidly during the envision stage.
(WF): The product owner and budget tend to be locked in pretty solidly during the envision stage.
(STARTIGNORE)
(Hint):
(Subject): Agile Adapt
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of these qualities tends to indicate a project that would be a good candidate for Agile?
(A): Expect requirements to change or evolve
(B): The product can deliver business value incrementally
(C): Done on a regular basis with a proven set of steps
(D): There is no business value in anything but the finished project deliverable
(E): The requirements are very strongly set, and unlikely to change
(Correct): A,B
(Points): 1
(CF): unvarying requirements and a proven process tend to be opposite the benefits of the Agile process.
(WF): unvarying requirements and a proven process tend to be opposite the benefits of the Agile process.
(STARTIGNORE)
(Hint):
(Subject): Agile
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which use case example summary is not valid?
(A): User interacts with atm machine to check account balance
(B): System administrator accesses settings in online tool to update user permissions
(C): System administrator explains online tool to user about password requirements
(D): System monitor process sends email to user about error condition
(Correct): C
(Points): 1
(CF): The administrator is not interacting with another actor rather than a system. A system is the primary point of the use case.
(WF): The administrator is not interacting with another actor rather than a system. A system is the primary point of the use case.
(STARTIGNORE)
(Hint):
(Subject): Agile Speculate
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): It is not vital for a sponsor to be 100% committed to the Agile process.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): A sponsor needs to be 100% committed to the process. This is vital.
(WF): A sponsor needs to be 100% committed to the process. This is vital.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): On a weekly basis, you may want to invite additional project stakeholders to the meeting.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): On a weekly basis, you may want to invite additional project stakeholders to the meeting.
(WF): On a weekly basis, you may want to invite additional project stakeholders to the meeting.
(STARTIGNORE)
(Hint):
(Subject): Speculating
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)




(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): One of the best characteristics of agile project management is the opportunity to obtain feedback frequently and apply changes based on what the team has learned.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): One of the best characteristics of agile project management is the opportunity to obtain feedback frequently and apply changes based on what the team has learned.
(WF): One of the best characteristics of agile project management is the opportunity to obtain feedback frequently and apply changes based on what the team has learned.
(STARTIGNORE)
(Hint):
(Subject): Adapting and Closing
(Difficulty): Beginner
(Applicability): Course 
(ENDIGNORE)
(Difficulty): Intermediate 
(Applicability): General
(ENDIGNORE)




(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not true about the Agile Lifecycle?
(A): The primary purpose of the Speculate phase is for the business and technical teams to identify the features for this iteration.
(B): The Adapt phase immediately follows the Explore Phase.
(C): At the end of the Envision stage, you should have a documented project charter.
(D): The Speculate phase is only performed once during a project.
(E): Stand-up Meetings are a component of the Explore phase.
(Correct): D
(Points): 1
(CF): Only the Envision and Close phase are completed only once.
(WF): Only the Envision and Close phase are completed only once.
(STARTIGNORE)
(Hint):
(Subject): Agile Lifecycle
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)




(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which statement is false about an Agile Project?
(A): Since a Sprint is time-bound, a Sprint has a limit of one feature.
(B): A story is a short description of functionality described from the perspective of a user or customer.
(C): Agile projects deliver value to the business in frequent small deliveries of product called features.
(D): Agile looks at the project as a collection of chunks.  Those chunks are divided by sprints and features.
(E): Values and Norms are a set of rules and guidelines for the Scrum Team to follow.
(Correct): A
(Points): 1
(CF): A Sprint is defined as a releasable iteration, specifically a collection of features.
(WF): A Sprint is defined as a releasable iteration, specifically a collection of features.
(STARTIGNORE)
(Hint):
(Subject): Scrum Artifacts
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)



(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which choice provides the best definition of Snowplowing?
(A): The practice of eliminating the testing phase in order to finish the project on time.
(B): Unimplemented features being added to the backlog list at the end of a sprint.
(C): Termination of an Agile project.
(D): Adding additional resources to a project.
(E): The creation of a new Sprint.
(Correct): B
(Points): 1
(CF): The definition of snowplowing is an unimplemented feature being added to the backlog list at the end of a sprint.
(WF): The definition of snowplowing is an unimplemented feature being added to the backlog list at the end of a sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Artifacts
(ENDIGNORE)



(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which role is the least accurate regarding the Scrum Team?
(A): The Scrum Team is the car itself, ready to speed along in whatever direction it is pointed.
(B): The Product Owner is the driver, making sure that the car is always going in the right direction.
(C): The Scrum Master is responsible for delegating tasks to the team.
(D): The Scrum team, despite their title, is responsible for ensuring the Sprint deadlines are met.
(E): The Scrum Master is the chief mechanic, keeping the car well-tuned and performing at its best.
(Correct): C
(Points): 1
(CF): The Scrum Master's role is not a delegator.
(WF): The Scrum Master's role is not a delegator.
(STARTIGNORE)
(Hint):
(Subject): Scrum Team
(Difficulty): Intermediate
(Applicability): General
(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of the following mark the close phase?
(A): You run out of funds
(B): You run out of time
(C): All features complete
(D): Business value has been attained for the first time in the project
(Correct): A, B, C
(Points): 1
(CF): 
(WF): With Agile, business value early and often.
(STARTIGNORE)
(Hint): Agile attempts to provide value early and often
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The Scrum master helps find techniques for effective product backlog management.
(A): True 
(B): False
(Correct): A
(Points): 1
(CF): The scrum master facilitates all parts of the process, including finding effective techiques for the product owner's use.
(WF): The scrum master facilitates all parts of the process, including finding effective techiques for the product owner's use.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)





(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): Scrum is difficult to understand, but simple to master.
(A): True	
(B): False
(Correct): B
(Points): 1
(CF): Scrum is actually SIMPLE to understand, but Difficult to master (although we know you wouldn't have a problem either way).
(WF): Scrum is actually SIMPLE to understand, but Difficult to master (although we know you wouldn't have a problem either way).
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum Teams deliver products iteratively and incrementally, minimizing opportunities for
feedback. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Scrum Teams deliver products iteratively and incrementally, maximizing opportunities for
feedback. 
(WF): Scrum Teams deliver products iteratively and incrementally, maximizing opportunities for
feedback. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The Sprint review is a
(A): status meeting
(B): formal meeting
(C): informal meeting
(D): decision making meeting
(Correct): C
(Points): 1
(CF): This is an informal meeting, not a status meeting, and the presentation of the Increment is intended to elicit feedback and foster collaboration.
(WF): This is an informal meeting, not a status meeting, and the presentation of the Increment is intended to elicit feedback and foster collaboration.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Scrum is founded on the Empirical Process Control Theory, in that Scrum focuses on...
(A): Completing projects in large segments, completing each section before moving on.
(B): Creating multiple approaches simultaneously and choosing the best at the end.
(C): Having a testing phase after each phase, only moving on after the end of the previous phase.
(D): Working on small segments in a short timeframe, adding on as new requirements emerge.
(E): A four phase cycle that is continually passed through until completion.
(Correct): D
(Points): 1
(CF): 
(WF): Scrum is an agile development framework, if focuses on small sprints, back to back, only changing the project details between sprints.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)
