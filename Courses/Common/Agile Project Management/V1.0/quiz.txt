﻿(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
2 Axure
6 Communication
0 Generic
8 HTML
1 Java
3 JIRA
4 OOP
5 SDLC
7 Unix Scripting

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Derrick Shriver
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)
(Type): multiplechoice
(Category): 5
(Question): Who directs the Development Team in how to complete their tasks?
(A): Scrum Master
(B): Both A and B
(C): Product Owner
(D): The Development Team
(Correct): D 
(Points): 1
(CF): Within the Scrum Team, the Development Team itself owns the process of completing their tasks.
(WF): Within the Scrum Team, the Development Team itself owns the process of completing their tasks.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate 
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which actions best fit the role of Scrum Master?
(A): Directing, Managing, Assessing
(B): Coaching, Teaching, Facilitating
(C): Prioritizing, Approving, Deciding
(D): Specifying, Evaluating, Requesting
(Correct): B
(Points): 1
(CF): Directing, etc.: No one on the Scrum Team does this.  Specifying, etc.: More the Product Owner.  Prioritizing, etc.: Also Product Owner.
(WF): Directing, etc.: No one on the Scrum Team does this.  Specifying, etc.: More the Product Owner.  Prioritizing, etc.: Also Product Owner.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5 
(Question): What is a burndown chart?
(A): A chart showing all of a project's use cases
(B): A chart that shows the hours remaining for the project
(C): A chart that shows the funding remaining for a project
(D): A chart which illustrates the work left to be completed
(E): A chart that shows the number of bugs that have been fixed
(Correct): D
(Points): 1
(CF): A burndown chart illustrates work left to be completed
(WF): A burndown chart illustrates work left to be completed
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): If a team is very well-versed in the Agile methodology, what is a good way to mitigate risk?
(A): Lengthen the Speculate phase of each sprint
(B): Move complex or difficult features to later sprints
(C): Make your sprints shorter as the project moves forward
(D): Complete complex or difficult features in early sprints
(E): Distribute complex or difficult features throughout all sprints
(Correct): D
(Points): 1
(CF): A team that is familiar with Agile can lower project risk by tackling difficult features first.
(WF): A team that is familiar with Agile can lower project risk by tackling difficult features first.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): What are some risks to consider with an agile project?
(A): The sprint schedule is overly ambitious
(B): Inexperienced members have trouble with processes
(C): Management will not be satisfied by half finished initial releases
(D): Team members are not willing to take risks and make decisions without approval
(Correct): A,B,C,D
(Points): 1
(CF): Process inexperience in both the Scrum Team and the Business can be mitigated with less ambitious early sprints.
(WF): Process inexperience in both the Scrum Team and the Business can be mitigated with less ambitious early sprints.
(STARTIGNORE)
(Hint):
(Subject): Agile
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which of these statements are correct for the 'fist of five' voting system?
(A): Each person is given 5 votes to hold on to for use during the meeting
(B): 5 team members should play rock paper scissors to determine scrum lead
(C): Each team member should hold up a hand with a number of fingers up to vote
(D): Subdividing larger project teams into subgroup of 5 can help improve sprint velocity
(Correct): C
(Points): 1
(CF): Fist of five is a voting system with an added level of detail over just a show of hands, allowing more detail on votes.
(WF): Fist of five is a voting system with an added level of detail over just a show of hands, allowing more detail on votes.
(STARTIGNORE)
(Hint):
(Subject): Agile Adapt
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): What is a feature?
(A): The final product.
(B): The issues the team runs into.
(C): A piece of functionality or outcome that is valued by the client.
(D): A piece of functionality or outcome that the team wants to include.
(Correct): C
(Points): 1
(CF): A feature is a piece of functionality or outcome that is valued by the client.
(WF): A feature is a piece of functionality or outcome that is valued by the client.
(STARTIGNORE)
(Hint):
(Subject): Understanding Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which definition of velocity is correct?
(A): Velocity measures the speed required to finish a project.
(B): Velocity measures the number of bugs created in a sprint.
(C): Velocity measures the speed it takes to complete one feature.
(D): Velocity measures the number of features that you complete in the product backlog.
(E): Velocity measures the number of points the team is completing in the average sprint.
(Correct): E 
(Points): 1
(CF): Velocity measures the number of points the team is completing in the average sprint.
(WF): Velocity measures the number of points the team is completing in the average sprint.
(STARTIGNORE)
(Hint):
(Subject): Definitions
(Difficulty): Intermediate 
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which is the least important element of an Agile project?
(A): Sprint
(B): User Stories
(C): Business Value
(D): Product Backlog
(E): Formal Documentation
(Correct): E
(Points): 1
(CF): Stakeholders are more interested in the product than the documentation.  Although some documentation such as a Project Charter is required, the actual product provides real business value.
(WF): Stakeholders are more interested in the product than the documentation.  Although some documentation such as a Project Charter is required, the actual product provides real business value.
(STARTIGNORE)
(Hint):
(Subject): Scrum Artifacts
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which is true for an Agile project?
(A): Writing documentation is a key component.
(B): The prioritization of features cannot be modified.
(C): All of the features of a product must be delivered in the same release.
(D): Requirements can and are expected to change at any time during the project.
(E): Subject Matter Experts and users do not need to be involved in an Agile project.
(Correct): D
(Points): 1
(CF): One of the key fundamentals and benefits of an Agile Project is the requirements can and will change at any time as more information is discovered during the course of the project.
(WF): One of the key fundamentals and benefits of an Agile Project is the requirements can and will change at any time as more information is discovered during the course of the project.
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)
(Type): multiplechoice
(Category): 5
(Question): Which statement is true about the role of the Product Owner?
(A): The Product Owner is responsible for testing the features
(B): The Product Owner is accountable for the Sprint Retrospectives
(C): Once the Agile project has completed the Speculate phase, they do not need to participate in the project until the beginning of the Close phase.
(D): The Product Owner creates a compelling vision of the product, and then conveying that vision to the team through the prioritized product backlog.
(Correct): D
(Points): 1
(CF): The Product Owner is active during the whole Agile Lifecycle.  For example, the role of the Product Owner is to continually monitor and reprioritize the Product Backlog.  This activity is a big part of the Speculate/Explore/Adapt phases.
(WF): The Product Owner is active during the whole Agile Lifecycle.  For example, the role of the Product Owner is to continually monitor and reprioritize the Product Backlog.  This activity is a big part of the Speculate/Explore/Adapt phases.
(STARTIGNORE)
(Hint):
(Subject): Scrum Team
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Why does the first iteration of the speculate phase take longer?
(A): Because it is likely no one has done Agile before
(B): Because you should always estimate bigger on the first try
(C): Because you are estimating all the features in the product
(D): Because you will have more features to do in the first sprint
(Correct): C
(Points): 1
(CF): 
(WF): On the first iteration, none of the features are estimated.  This means that all of them need to be estimated before you can move on to the next phase.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Who looks at future sprints to confirm proper business alignment?
(A): Scrum master
(B): Project Manager
(C): Development Team
(D): Business Analyst / Product Owner
(Correct): D
(Points): 1
(CF): 
(WF): The business analyst or Product Owner is best suited to see misalignment with the business.
(STARTIGNORE)
(Hint): The core team should not be looking far ahead
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): What are two methods to add energy to Daily Scrum? (Choose two.)
(A): Keep the team standing during the meeting
(B): Change the order of who gives their status
(C): Resolve issues as soon as they are mentioned
(D): Have the meeting at a different time everyday
(Correct):  A, B
(Points): 1
(CF): 
(WF): Consistency is key to these meetings.  The meeting should not be interrupted to resolve issues.  This needs to happen after.
(STARTIGNORE)
(Hint): Consistency
(Subject): SDLC
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): What two approaches can be used when co-location is not possible? (Choose two.)
(A): Abandon an Agile approach
(B): Have frequent digital meetings
(C): Supply necessary collaborative tools (e.g. SharePoint)
(D): Assume the work done by remote employees will be inferior and plan accordingly 
(Correct): B, C
(Points): 1
(CF): While more difficult, Agile without co-location is possible
(WF): Agile without co-location is possible and remote employees can be just as valuable
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(STARTIGNORE)
Scrum Guide Questions Start Here ----------------------------------------------------------
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): What are some of the primary Scrum specific events?
(A): Daily Scrum
(B): Sprint Planning
(C): Daily Retrospective
(D): Sprint Backlog Review
(Correct): A,B
(Points): 1
(CF): You only do a retrospective at the end of a sprint, and you do not review sprint backlog in any specific event.
(WF): You only do a retrospective at the end of a sprint, and you do not review sprint backlog in any specific event.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): The product owner has control of which items? (Select two)
(A): Who attends Sprint Reviews
(B): Priority order of items in product backlog
(C): Content and details of product backlog items
(D): The order in which items are worked on within a sprint
(E): The number of backlog items to be completed in a sprint
(Correct): B,C
(Points): 1
(CF): Only the development team controls how many items they can complete in a sprint, otherwise the product owner controls most backlog functions.
(WF): Only the development team controls how many items they can complete in a sprint, otherwise the product owner controls most backlog functions.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Question): Sprint cancellation is rare, what might cause such a cancellation?
(A): The sprint goal is no longer relevant to the company's direction
(B): Re-estimated scope of backlog items is too large to fit in sprint
(C): The scrum master feels that the team will not make the sprint goal
(D): A team member leaves the company and a replacement cannot be found
(Correct): A
(Points): 1
(CF): Failing to meet the goals of a sprint will not cancel the sprint, including being down by a member.  Only the goal becoming obsolete can cancel a sprint.
(WF): Failing to meet the goals of a sprint will not cancel the sprint, including being down by a member.  Only the goal becoming obsolete can cancel a sprint.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which of the following is important to keep the same in the daily scrum?
(A): Location
(B): Order of presentation
(C): Whether observers are present
(Correct): A
(Points): 1
(CF): It is actually helpful to vary the order to keep people more attentive. The rest help reduce complexity.
(WF): It is actually helpful to vary the order to keep people more attentive. The rest help reduce complexity.
(STARTIGNORE)
(Hint):
(Subject): Agile Scrum
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): During a Daily Scrum (Standup), which three questions does each member of the Development Team answer? (Choose three)
(A): What will I do today to help met the Spring Goal.
(B): What did I do since we last met that helped meet the Sprint Goal.
(C): Details on how I think another team member's problem can be solved.
(D): What did I do at the beginning of the week that helped meet the Sprint Goal.		
(E): Do I see any impediment that prevents me or the Development Team from meeting the Sprint Goal.
(Correct): B,E,A
(Points): 3
(CF): A, C, and D are correct
(WF): A, C, and D are correct
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which event provides an opportunity for the Scrum Team to inspect itself and create an improvement plan for next Sprint?
(A): Daily Scrum
(B): Sprint Review
(C): Sprint Planning		
(D): Sprint Retrospective
(E): Product Owner Meeting
(Correct): D
(Points): 1
(CF): Correct. The Sprint Retrospective meeting is held for the Scrum Team to inspect itself and create an improvement plan for next Sprint.
(WF): The Sprint Retrospective meeting is held for the Scrum Team to inspect itself and create an improvement plan for next Sprint.
(STARTIGNORE)
(Hint):
(Subject): Question Subject
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): What is allowed to change during the course of a sprint?
(A): Project Scope
(B): Quality goals
(C): Product goals
(D): Product backlog
(E): Priority order of features within the sprint
(Correct): A,D
(Points): 1
(CF): The scope of the project, product backlog (changed by the product owner), and the functionality of the product can change during a sprint.
(WF): The scope of the project, product backlog (changed by the product owner), and the functionality of the product can change during a sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Advanced
(Applicability): general
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Suppose you are a developer, and a Manager of another team informs you that there a bug in your backlog that needs to be fixed immediately due to a road block.  What is a proper course of action?
(A): Fix the bug immediately
(B): Address the Product Owner
(C): Edit the backlog and promote the bug to a higher priority
(D): Wait until the next backlog grooming to address the problem
(Correct): B
(Points): 1
(CF): Correct. No one is allowed to tell the Development Team to work from a different set of requirements, and the Development Team isn’t allowed to act on what anyone else says.
(WF): While the development team should not act on what an external manager says, the Product Owner needs to be informed and their decision respected.

(Type): multiplechoice
(Category): 5
(Question): Which event lies outside of a sprint?
(A): Daily Scrum
(B): None of these
(C): Sprint Review
(D): Sprint Retrospective
(Correct): B
(Points): 1
(CF): Correct. All events are contained in the sprint.
(WF): A new Sprint begins right after one ends.  Nothing is outside of it.

(Type): multiplechoice
(Category): 5 
(Question): Which two statements describe the development team? (Choose two)
(A): It does not test
(B): It is self-organizing
(C): It is cross-functional
(D): It is divided into two sub-teams
(E): It has a lead developer who specifies roles and tasks
(Correct): B, C
(Points): 1
(CF): Development Teams are cross-functional, self-organizing, and responsible for carrying out the work necessary to release an increment each sprint.
(WF): Development Teams are cross-functional, self-organizing, and responsible for carrying out the work necessary to release an increment each sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): When should the development team deliver a working product?
(A): After every sprint
(B): Sometime in the last two sprints
(C): Mid-way through the project lifespan
(D): Whenever the project stakeholders demand it
(E): At the sprint review for the project's final sprint
(Correct): A
(Points): 1
(CF): Due to the scrum method's iterative and incremental nature, some form of a working product is available after each sprint.
(WF): Due to the scrum method's iterative and incremental nature, some form of a working product is available after each sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum Guide
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Select the three pillars that the empirical process consists of.
(A): Recursion
(B): Adaptation
(C): Inspection
(D): Obfuscation
(E): Transparency
(Correct): B,C,E
(Points): 1
(CF): 
(WF): Transparency or having all aspects defined by a common standard to promote a common understanding. Inspection, this is used to find any undesirable elements during or after a sprint. After an inspection if an undesirable element is found the project must adapt to change to remove the unwanted elements. 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): The members that make up the Scrum Team are?
(A): Team Owner
(B): Scrum Master
(C): Product Owner
(D): Development Team
(Correct): B,C,D
(Points): 1
(CF): 
(WF): The three parts that make the Scrum team are, the Product Owner, the Development Team and the Scrum Master.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which statement is true about Planning Poker?
(A): A prime number sequence is often used
(B): Only one suit is used from a normal deck of cards
(C): Poker chips represent priority and card value represents effort
(D): All cards are selected individually in private and revealed simultaneously.
(E): If the estimators do not agree after five times then the estimate with the majority is selected.
(Correct): D
(Points): 1
(CF): 
(WF): With Planning Poker, each participant has a special deck of cards numbered typically using a Fibbonocci-like sequence such as 1,2,3,5,8,15,30,Very Large. There are no poker chips.  All participants reveal their card simultaneously in order to reduce cross-participant influence.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Question): Which two of these are true about User Stories? (Choose two)
(A): A User Story should utilize technical language.
(B): User Stories end with a summary of the main points of the Story.
(C): User Stories should be rich with information to describe the feature.
(D): Details can be added to user stories by adding conditions of satisfaction.
(E): The typical template of a User Story is, As a <type of user>, I want <some goal> so that <some reason>.
(Correct): D,E
(Points): 1
(CF): 
(WF): A User Story is a short simple description of a feature told from the perspective of the person who desires the new capability.  There is no summary; it is a summary.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)