(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tsetsen erdene Ganbaatar
(Course Site): Lynda
(Course Name): SQL Essential Training
(Course URL): http://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): To aggregate rows before performing a query operation on them, what command is used?
(A): GROUP BY
(B): ORDER BY
(C): SORT BY
(D): EXECUTE BY
(E): COMPOSE BY
(Correct): A
(Points): 1
(CF): The GROUP BY X syntax, where X is some column, aggregates rows before operations are performed on them, meaning table rows with matching X get treated as one row.
(WF): The GROUP BY X syntax, where X is some column, aggregates rows before operations are performed on them, meaning table rows with matching X get treated as one row.
(STARTIGNORE)
(Hint):
(Subject): SQL Fundamentals
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Since not all SQL dialects support Right join, a Right Join can generally be performed by switching the left and right tables and then performing a left join.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): A Right Join is the converse of a Left Join. This is useful when working with a variant of SQL that does not formally support right joins.
(WF): A Right Join is the converse of a Left Join. This is useful when working with a variant of SQL that does not formally support right joins.
(STARTIGNORE)
(Hint):
(Subject): SQL Fundamentals
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 1
(Random answers): 1
(Question): For the command "SELECT Region, COUNT(*) From Country GROUP BY Region ORDER BY Region DESC", which of these statement is true (select two)
(A): The query counts how many countries there are by region.
(B): The query counts how many regions there are.
(C): The query groups the regions in a descending order.
(D): The query displays 3 columns: Region, Count(*) and Country
(E): The query gets its results from the Country table.
(Correct): A, E
(Points): 1
(CF): This query displays the Regions and how many countries are in each region, sorted by descending region names.
(WF): This query displays the Regions and how many countries are in each region, sorted by descending region names.
(STARTIGNORE)
(Hint):
(Subject): SQL Fundamentals
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): An SQL sub-select can generally be treated as a SELECT statement nested within another SELECT statement.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The result of a SELECT statement is effectively a table and so the result can be passed as input into another SELECT statement.
(WF): The result of a SELECT statement is effectively a table and so the result can be passed as input into another SELECT statement.
(STARTIGNORE)
(Hint):
(Subject): SQL Fundamentals
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): For databases with elements that have many to many relationships, what SQL feature is often used to accomplish this?
(A): Junction Tables.
(B): Nested Tables.
(C): Joined Databases.
(D): Composite Tables.
(E): Corollary Tables.
(Correct): A
(Points): 1
(CF): A Junction table is a utility table that can link many tables and perform several roles.
(WF): A Junction table is a utility table that can link many tables and perform several roles.
(STARTIGNORE)
(Hint):
(Subject): SQL Fundamentals
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)